# TERRAFORM-GKE

[EN version](../README.md)

Данный проект позволяет  `google kubernetes engine` с помощью `terraform` и автоматизировать данный процесс с помошью `gitlab pipeline`.

Пул узлов по умолчанию удаляется после первичного создания кластера для большей гибкости в дальнейшем использовании. Новый пул узлов создается как отдельный обьект, для того чтобы при удалении узлов не приходилось удалять весь кластер. Сервисы мониторинга и логирования предложенные `GKE` отключаются, для возможности выбора других сервисов. В качестве сетевого плагина используется `Calico`.

---

## Использование


### 1. Запуск и создания инфраструктуры c помощью gitlab

* необходимо создать `gitlab CI/CD` переменные

---
| Name   |  Description | Example |
| ------ | ------------ | ------- |
| gcp_creds | authentification json file (необходимо закодировать в base64). | https://cloud.google.com/iam/docs/creating-managing-service-account-keys#creating_service_account_keys |
| terraform_tfvars| terraform.tfvars для данного проекта. | ./terraform.tfvars.example |

---

* процесс создания переменных можно автоматизировать с помощью данного проекта - [SOMikhaylov/terraform-gitlab-vars](https://gitlab.com/SOMikhaylov/terraform-gitlab-vars)

### 2. Для запуска и создания инфраструктуры на локальной машине

необходимо заполнить файл `terraform.tfvars` в соответсвии с вашими настройками

```
cp terraform.tfvars.example terraform.tfvars
```

* инициализировать `terraform` для использования удаленного `backend`

```
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>/lock" \
    -backend-config="username=<YOUR-USERNAME>" \
    -backend-config="password=<YOUR-ACCESS-TOKEN>" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

* для запуска инфраструктуры
```
terraform plan
terraform apply -auto-approve 
```

### 3. Для запуска и создания инфраструктуры на локальной машине без удаленного бэкенда

Если вы хотите хранить файл `terraform.state`  только на вашей локальной машине, вам необходимо выполнить данные команды 

```
cp backend.tf backend.tf.example 
```

необходимо заполнить файл terraform.tfvars в соответсвии с вашими настройками

```
cp terraform.tfvars.example terraform.tfvars
```

```
cp terraform.tfvars.example terraform.tfvars

terraform init 
terraform plan
terraform apply -auto-approve 
```
### 4. Удаление инфраструктуры
```
terraform destroy -auto-approve 
```

## Используемые инструменты

 * terraform
 * google-cloud-sdk

---

## Установка

вы можете установить все данные инструменты на вашу локальную машину с помощью данных гайдов 

* [Google Cloud sdk](https://cloud.google.com/sdk/docs/install)
* [Terraform Install](https://learn.hashicorp.com/tutorials/terraform/install-cli)


## Смотрите также

- [Terraform Google Cloud Platform Provider](https://registry.terraform.io/providers/hashicorp/google/latest/docs)

## Лицензия

MIT