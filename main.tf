resource "google_service_account" "default" {
  account_id   = "service-account-id"
  display_name = "Service Account"
}

data "google_container_engine_versions" "engine_versions" {
}

resource "google_container_cluster" "primary" {
  name                     = "gke-cluster"
  location                 = var.gcp_zone
  initial_node_count       = 1
  remove_default_node_pool = true

  monitoring_service = "none"
  logging_service    = "none"

  min_master_version = data.google_container_engine_versions.engine_versions.latest_node_version
  node_version       = data.google_container_engine_versions.engine_versions.latest_node_version

  network_policy {
    enabled  = true
    provider = "CALICO"
  }
}

resource "google_container_node_pool" "infra_pool" {
  name       = "infra-pool"
  location   = var.gcp_zone
  cluster    = google_container_cluster.primary.name
  node_count = var.gke_infra_count

  node_config {
    preemptible  = true
    machine_type = var.gcp_machine_type
    image_type   = var.gcp_image
    disk_size_gb = var.gcp_disk_size

    service_account = google_service_account.default.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    taint = [
      {
        effect = "NO_SCHEDULE"
        key    = "node-role"
        value  = "infra"
      }
    ]
  }

}

resource "google_container_node_pool" "app_pool" {
  name       = "app-pool"
  location   = var.gcp_zone
  cluster    = google_container_cluster.primary.name
  node_count = var.gke_app_count

  node_config {
    preemptible  = true
    machine_type = var.gcp_machine_type
    image_type   = var.gcp_image
    disk_size_gb = var.gcp_disk_size

    service_account = google_service_account.default.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}