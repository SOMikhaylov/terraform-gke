terraform {
  required_version = ">= 0.13"
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.63.0"
    }
  }
}

provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
  zone    = var.gcp_zone
}