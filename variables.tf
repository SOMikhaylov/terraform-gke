variable "gcp_project" {
  description = "your GCP project "
  type        = string
}

variable "gcp_region" {
  description = "This is your GCP region"
  type        = string
}

variable "gcp_zone" {
  description = "This is your GCP zone"
  type        = string
}

variable "gcp_image" {
  description = "gcp image"
  type        = string
}

variable "gcp_machine_type" {
  description = "gcp machine type"
  type        = string
}

variable "gcp_disk_size" {
  description = "gcp image size in GB"
  type        = string
}

variable "gke_infra_count" {
  description = "gke infra pool node count"
  type        = number
}

variable "gke_app_count" {
  description = "gke app pool node count"
  type        = number
}