# TERRAFORM-GKE

[RU version](docs/RU_README.md)

This project allows creating `google kubernetes engine` with `terraform` and automate this process with `gitlab pipeline`.

Default node pool removes after primary cluster creating  for much flexibility in the furthest using. A New node pool creates such as a separate object, therefore, all the cluster doesn’t need to be destroyed for removing nodes. Monitoring and logging services offered by `GKE` are turned off, for opportunity choose other services. Also, used network plugin `Calico`.

---

## Usage


### 1. Running and creating infrastructure with gitlab

* need to create `gitlab CI/CD` variables

---
| Name   |  Description | Example |
| ------ | ------------ | ------- |
| gcp_creds | authentification json file (need to endcode to base64). | https://cloud.google.com/iam/docs/creating-managing-service-account-keys#creating_service_account_keys |
| terraform_tfvars| terraform.tfvars for this project. | ./terraform.tfvars.example |

---

* process of creating variables might automate with this project - [SOMikhaylov/terraform-gitlab-vars](https://gitlab.com/SOMikhaylov/terraform-gitlab-vars)

### 2. For running and creating infrastructure on local machine

need to fill in `terraform.tfvars` file with acording to your settings

```
cp terraform.tfvars.example terraform.tfvars
```

* initialize `terraform` for using remote `backend`

```
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>/lock" \
    -backend-config="username=<YOUR-USERNAME>" \
    -backend-config="password=<YOUR-ACCESS-TOKEN>" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

* for running infrastructure
```
terraform plan
terraform apply -auto-approve 
```

### 3. For running and creating infrastructure on local machine without remote backend

If you want to store `terraform.state` file  only on your local machine, you need to perform these commands

```
cp backend.tf backend.tf.example 
```

need to fill in `terraform.tfvars` file with acording to your settings

```
cp terraform.tfvars.example terraform.tfvars
```

```
cp terraform.tfvars.example terraform.tfvars

terraform init 
terraform plan
terraform apply -auto-approve 
```

### 4. Deleting infrastructure
```
terraform destroy -auto-approve 
```

## Using tools

 * terraform
 * google-cloud-sdk

---

## Installation

you may install all of the tools with these guides 

* [Google Cloud sdk](https://cloud.google.com/sdk/docs/install)
* [Terraform Install](https://learn.hashicorp.com/tutorials/terraform/install-cli)


## See also

- [Terraform Google Cloud Platform Provider](https://registry.terraform.io/providers/hashicorp/google/latest/docs)

## License

MIT